class Fixnum
    DIGITS = { 0 => "zero", 1 => "one", 2 => "two",
      3 => "three", 4 => "four", 5 => "five",
      6 => "six", 7 => "seven", 8 => "eight",
      9 => "nine", 10 => "ten", 11 => "eleven",
      12 => "twelve", 13 => "thirteen",
      14 => "fourteen", 15 => "fifteen",
      16 => "sixteen", 17 => "seventeen",
      18 => "eighteen", 19 => "nineteen",
      20 => "twenty", 30 => "thirty", 40 => "forty",
      50 => "fifty", 60 => "sixty", 70 => "seventy",
      80 => "eighty", 90 => "ninety",
      100 => "hundred", 1000 => "thousand",
      1000000 => "million", 1000000000 => "billion",
      1000000000000 => "trillion"}


  def in_words
    len = self.to_s.chars.length
    if len < 3
      return self.from_0_to_99
    elsif len >= 3 && len < 4
      return self.from_100_to_999
    elsif len >= 4 && len < 7
      return self.from_1000_to_999999
    elsif len >= 7 && len < 10
      return self.from_million_to_billion
    elsif len >= 10 && len < 13
      return self.from_billion_to_trillion
    else
      return self.from_trillion_to_infinity
    end

  end

  def from_0_to_99
    if DIGITS[self] == nil
      return "#{DIGITS[self / 10 * 10]} #{DIGITS[self % 10]}"
    else
      DIGITS[self]
    end
  end

  def from_100_to_999
    if self < 99
      return "#{self.from_0_to_99}"
    end
    rest = self - (self / 100 * 100)
    if rest == 0
      return "#{DIGITS[self / 100]} #{DIGITS[100]}"
    else
      return "#{DIGITS[self / 100]} #{DIGITS[100]} #{rest.from_0_to_99}"
    end
  end

  def from_1000_to_999999
    if self < 99
      return "#{self.from_0_to_99}"
    end
    rest = self - (self / 1000 * 1000)
    if rest == 0
      return "#{DIGITS[self / 1000]} #{DIGITS[1000]}"
    else
      if (self / 1000) < 100
        return "#{(self / 1000).from_0_to_99} #{DIGITS[1000]} #{rest.from_100_to_999}"
      else
        return "#{(self / 1000).from_100_to_999} #{DIGITS[1000]} #{rest.from_100_to_999}"
      end
    end
  end

  def from_million_to_billion
    if self < 99
      return "#{self.from_0_to_99}"
    end
    rest = self - (self / 1000000 * 1000000)
    if rest == 0
      return "#{DIGITS[self / 1000000]} #{DIGITS[1000000]}"
    else
      if (self / 1000000) < 100
        return "#{(self / 1000000).from_0_to_99} #{DIGITS[1000000]} #{rest.from_1000_to_999999}"
      else
        return "#{(self / 1000000).from_100_to_999} #{DIGITS[1000000]} #{rest.from_1000_to_999999}"
      end
    end
  end


  def from_billion_to_trillion
    if self < 99
      return "#{self.from_0_to_99}"
    end
    rest = self - (self / 1000000000 * 1000000000)
    if rest == 0
      return "#{DIGITS[self / 1000000000]} #{DIGITS[1000000000]}"
    else
      if (self / 1000000000) < 100
        return "#{(self / 1000000000).from_0_to_99} #{DIGITS[1000000000]} #{rest.from_million_to_billion}"
      else
        return "#{(self / 1000000000).from_100_to_999} #{DIGITS[1000000000]} #{rest.from_million_to_billion}"
      end
    end
  end

  def from_trillion_to_infinity
    if self < 99
      return "#{self.from_0_to_99}"
    end
    rest = self - (self / 1000000000000 * 1000000000000)
    if rest == 0
      return "#{DIGITS[self / 1000000000000]} #{DIGITS[1000000000000]}"
    else
      return "#{(self / 1000000000000).from_0_to_99} #{DIGITS[1000000000000]} #{rest.from_billion_to_trillion}"
    end
  end
  # def in_words
  #   n = self
  #
  #   words_hash = {0=>"zero",1=>"one",2=>"two",3=>"three",4=>"four",5=>"five",6=>"six",7=>"seven",8=>"eight",9=>"nine",
  #                   10=>"ten",11=>"eleven",12=>"twelve",13=>"thirteen",14=>"fourteen",15=>"fifteen",16=>"sixteen",
  #                    17=>"seventeen", 18=>"eighteen",19=>"nineteen",
  #                   20=>"twenty",30=>"thirty",40=>"forty",50=>"fifty",60=>"sixty",70=>"seventy",80=>"eighty",90=>"ninety"}
  #
  #    scale = [000=>"",1000=>"thousand",1000000=>" million",1000000000=>" billion",1000000000000=>" trillion", 1000000000000000=>" quadrillion"]
  #
  #
  #   if words_hash.has_key?(n)
  #     words_hash[n]
  #
  #     #still working on this middle part. Anything above 999 will not work
  #    elsif n>= 1000
  #    print  n.to_s.scan(/.{1,3}/) do |number|
  #           print number
  #     end
  #
  #
  #
  #     #print value = n.to_s.reverse.scan(/.{1,3}/).inject([]) { |first_part,second_part| first_part << (second_part == "000" ? "" : second_part.reverse.to_i.in_words) }
  #     #(value.each_with_index.map { |first_part,second_part| first_part == "" ? "" : first_part + scale[second_part] }-[""]).reverse.join(" ")
  #
  #   elsif n <= 99
  #      return [words_hash[n - n%10],words_hash[n%10]].join(" ")
  #   else
  #     words_hash.merge!({ 100=>"hundred" })
  #     ([(n%100 < 20 ? n%100 : n.to_s[2].to_i), n.to_s[1].to_i*10, 100, n.to_s[0].to_i]-[0]-[10])
  #       .reverse.map { |num| words_hash[num] }.join(" ")
  #   end
  # end
end
